﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Services;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers
{
    public class PartnersControllerTests
    {
        private Partner GetBasePartner()
        {
            var partner = PartnerBuilder.CreateBasePartner();

            return partner;
        }
        
        /// <summary>
        /// Тест на состояние
        /// </summary>
        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        [InlineData("7d994823-8226-4273-b063-1a95f3cc1df8")]
        public async void CanNotSetNotActivePartnerPromoCodeLimitAsync(string partnerIdStr)
        {
            // Arrange
            var partnerId = Guid.Parse(partnerIdStr);
            var partner = GetBasePartner().WithNotActiveLimit(); 

            var request = new SetPartnerPromoCodeLimitRequest();

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(mock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
        
        /// <summary>
        /// Тест на поведение
        /// </summary>
        [Fact]
        public async void CanSetActivePartnerPromoCodeLimitAsyncAndGotPartnerFromRepository()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest();

            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            var partnerPromoCodeLimitManagerMock = new Mock<IPartnerPromoCodeLimitManager>();
            partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(It.IsAny<Partner>());
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(partnerRepositoryMock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            partnerRepositoryMock.Verify(x=> x.GetByIdAsync(partnerId), Times.Once);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = GetBasePartner().WithNotActiveLimit(); 
            
            var request = new SetPartnerPromoCodeLimitRequest();
            
            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(mock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task CancelPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldSetCancelDateNow()
        {
            //Arrange
            var partner = GetBasePartner().WithOnlyOneActiveLimit();
            var targetLimit = partner.PartnerLimits.First();
            var partnerId = partner.Id;
            var now = new DateTime(2020, 10, 14);

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(now);
            var controller = new PartnersController(mock.Object);

            await controller.CancelPartnerPromoCodeLimitAsync(partnerId);
            
            targetLimit.Should().BeEquivalentTo(now);
        }
    }
}