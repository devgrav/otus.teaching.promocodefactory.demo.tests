﻿using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement
{
    public class PartnerPromoCodeLimitManagerTests
    {
        [Fact]
        public Task CancelPartnerPromoCodeLimitAsync_PartnerIsActive_ShouldThrowException()
        {
            return Task.CompletedTask;
        }
    }
}