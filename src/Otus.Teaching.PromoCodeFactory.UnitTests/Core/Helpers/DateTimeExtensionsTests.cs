﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Helpers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Helpers
{
    public class DateTimeExtensionsTests
    {
        public static readonly object[][] TheoryData =
        {
            new object[] {new DateTime(2017,3,1), "01.03.2017"},
            new object[] {new DateTime(2020,8,2), "02.08.2020"}
        };
        
        [Theory, MemberData(nameof(TheoryData))]
        public void ToStandartFormat_WithValidDates_ReturnsExpected(DateTime targetDate, string expectedDate)
        {
            //Act
            var actualDate = targetDate.ToStandartFormat();

            //Assert

            actualDate.Should().Be(expectedDate);
        }
    }
}