﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Exceptions
{
    public class PartnerNotActiveException
        : Exception
    {
        public PartnerNotActiveException(string message)
            : base(message)
        {
            
        }
    }
}