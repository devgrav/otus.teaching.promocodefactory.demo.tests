﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Services
{
    public class PartnerPromoCodeLimitManager
        : IPartnerPromoCodeLimitManager
    {
        private readonly IRepository<Partner> _partnerRepository;

        public PartnerPromoCodeLimitManager(IRepository<Partner> partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        
        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnerRepository.GetByIdAsync(id);
            
            if (partner == null)
                throw new EntityNotFoundException("Партнер не найден");
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new PartnerNotActiveException("Партнер заблокирован");
            
            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }

            await _partnerRepository.UpdateAsync(partner);
        }
    }
}